#include <boost/lexical_cast.hpp>
#include "hud.hpp"
#include "scene.hpp"
#include "game_rules.hpp"

namespace {
	struct is_coin{
		bool operator()(object const& obj) const {
			return obj.name() == "coin";
		};
	};
}

hud::hud(scene const& s, sf::VideoMode const& video_mode, game_rules const& rules):
	scene_(s), video_mode_(video_mode), rules_(rules) {

	map::objects_t const& objects = scene_.get_map().objects();
	map::objects_t::const_iterator const coin = std::find_if(objects.begin(), objects.end(), is_coin());
	assert(coin != objects.end() && "No coin found in map");
	coin_sprite_ = coin->sprite();

	std::string const font_path("8bitlim.ttf");
	if(!font_.loadFromFile(font_path))
		throw std::runtime_error(("No such font " + font_path).c_str());

	reset();
}

void hud::render(sf::RenderWindow& window) {
	switch(rules_.game_state()) {
		case game_rules::playing:
			render_coin_counter(window);
			break;
		case game_rules::lost:
			render_pretty_caption(window, "PLAYTIME IS OGRE", sf::Color::Red, 50);
			render_pretty_score_counter(window);
			render_tip_caption(window);
		break;
		case game_rules::won:
			render_pretty_caption(window, "YOU WON THE INTERNET", sf::Color::Green, 40);
			render_pretty_score_counter(window);
			render_tip_caption(window);
		break;
	}
}

void hud::render_coin_counter(sf::RenderWindow& window) {
	sf::Vector2i const coin_pos(scene_.get_camera().x() + video_mode_.width / 4 - coin_sprite_.getLocalBounds().width * 3, 
								scene_.get_camera().y() - static_cast<int>(video_mode_.height) / 4 + coin_sprite_.getLocalBounds().height);
	coin_sprite_.setPosition(coin_pos.x, coin_pos.y);
	window.draw(coin_sprite_);

	static sf::Text x("x", font_, 15);
	x.setPosition (coin_pos.x + coin_sprite_.getLocalBounds().width + 4, coin_pos.y - 1);
	window.draw(x);

	sf::Text score(boost::lexical_cast<std::string>(scene_.get_player().coins()), font_, 15);
	score.setPosition (x.getGlobalBounds().left + x.getGlobalBounds().width + 4, coin_pos.y);
	window.draw(score);
}

void hud::render_pretty_caption(sf::RenderWindow& window, std::string const& caption_str, sf::Color const& color, std::size_t size) {
	sf::Text caption(caption_str, font_, size);
	caption.setPosition(scene_.get_camera().x() - caption.getLocalBounds().width / 2, 
						scene_.get_camera().y() - video_mode_.height / 16 - caption.getLocalBounds().height);
	caption.setColor(color);
	window.draw(caption);
}

void hud::render_pretty_score_counter(sf::RenderWindow& window) {
	if(nth_coin_ < scene_.get_player().coins() * 500) {
		// if(pretty_score_clock_.getElapsedTime().asMilliseconds() > 1) 
		{
			nth_coin_ += 25;

			// pretty_score_clock_.restart();
		}
	}

	static sf::Text score("SCORE ", font_, 15);
	score.setPosition(scene_.get_camera().x() - score.getLocalBounds().width,
					  scene_.get_camera().y() - video_mode_.height / 32);
	window.draw(score);

	sf::Text nth_coin(boost::lexical_cast<std::string>(nth_coin_), font_, 15);
	nth_coin.setColor(sf::Color::Yellow);
	nth_coin.setPosition(score.getGlobalBounds().left + score.getGlobalBounds().width, score.getGlobalBounds().top - 1);
	window.draw(nth_coin);
}

void hud::render_tip_caption(sf::RenderWindow& window) {
	static sf::Text tip("Press R to restart the game.", font_, 15);
	tip.setPosition(scene_.get_camera().x() - tip.getLocalBounds().width / 2,
					scene_.get_camera().y() + video_mode_.height / 5);
	window.draw(tip);
}


void hud::reset() {
	nth_coin_ = 0;
}