#include "game_rules.hpp"
#include "scene.hpp"

game_rules::game_rules():
	state_(playing) {
}

void game_rules::update(scene& s) {
	if(s.get_player().rect().top >  s.get_map().height_px()) {
		state_ = lost;
	}
	if(collision_info const info = s.check_collision(s.get_player())) {
		if(info.obj->collision_type() == object::end_point) {
			state_ = won;
			s.remove_object(info.obj);
		}
	}
}

game_rules::game_state_t game_rules::game_state() const {
	return state_;
}

void game_rules::reset() {
	state_ = playing;
}