#include "object.hpp"

xml_object::xml_object(std::string _name, std::string _type, properties_t const& _properties):
	name(_name), type(_type), properties(_properties) {
}

object::collision_type_t type_by_name(std::string const& name) {
	if(name == "block")
		return object::solid;
	else if (name == "enemy")
		return object::solid;
	else if(name == "player")
		return object::solid;
	else if(name == "coin")
		return object::coin;
	else if(name == "end_point")
		return object::end_point;

	assert(false && "Unknown object type");
}

object::object(xml_object const& xml_object, sf::Sprite const& sprite, sf::Rect<int> const& rect):
	xml_object_(xml_object), sprite_(sprite), rect_(rect), collision_type_(type_by_name(xml_object_.name)){
}

std::string object::name() const {
	return xml_object_.name;
}

sf::Rect<int> object::rect() const {
	return rect_;
}

object::collision_type_t object::collision_type() const {
	return collision_type_;
}

void object::render(sf::RenderWindow &window) {
	window.draw(sprite_);
}

sf::Sprite object::sprite() const {
	return sprite_;
}