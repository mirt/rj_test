#ifndef SCNENE_HPP_
#define SCNENE_HPP_

#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include "map.hpp"
#include "player.hpp"

typedef boost::shared_ptr<player> player_ptr;

class camera {
public:
	camera(sf::Vector2i const& screen_size, sf::RenderWindow& window);
	void update();
	void set_target(player_ptr player);

	int x() const;
	int y() const;
private:
    sf::View view_;
    sf::Vector2i screen_size_;
    sf::RenderWindow& window_;
    player_ptr target_;
	sf::Vector2f pos_;
};

typedef std::list<object> objects_t;

struct collision_info {
	enum hit_place{none, bottom, other};

	bool colided;
	hit_place place;
	objects_t::iterator obj;

	operator bool() const;
	static collision_info collision(hit_place _place, objects_t::iterator _obj);
	static collision_info no_collision();
private:
	explicit collision_info(bool _colided);
	explicit collision_info(bool _colided, hit_place _place, objects_t::iterator _obj);
};

class scene {
public:
	scene(map const& m, camera& cam);

	void render(sf::RenderWindow &window);
	void handle_event(sf::Event const& event);
	void update(sf::Time const& delta);

	void reload(map const& m);

	collision_info check_collision(object const& obj);

	void remove_object(objects_t::iterator it);
	map const& get_map() const;
	camera const& get_camera() const;

	player const& get_player() const;
	player& get_player();
private:
	map const& map_;
	player_ptr player_;
	objects_t objects_;
	camera& camera_;
};

#endif