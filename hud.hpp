#ifndef HUD_HPP_
#define HUD_HPP_

#include <SFML/Graphics.hpp>

class scene;
class game_rules;

class hud {
public:
	explicit hud(scene const& s, sf::VideoMode const& video_mode, game_rules const& rules);
	void render(sf::RenderWindow& window);
	void reset();
private:
	void render_coin_counter(sf::RenderWindow& window);
	void render_pretty_score_counter(sf::RenderWindow& window);
	void render_pretty_caption(sf::RenderWindow& window, std::string const& caption_str, sf::Color const& color, std::size_t size);
	void render_tip_caption(sf::RenderWindow& window);
	scene const& scene_;
	sf::Sprite coin_sprite_;
	sf::Font font_;
	sf::VideoMode const& video_mode_;
	game_rules const& rules_;

	// sf::Clock pretty_score_clock_;
	std::size_t nth_coin_;
};

#endif