#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "object.hpp"

class scene;
class collision_info;

class player: public object {
public:
	player(object const& obj, scene& _scene);

	void render(sf::RenderWindow &window);
	void handle_event(sf::Event const& event);
	void update(sf::Time const& delta);

	void handle_coin(collision_info const& info);
	std::size_t coins() const;
	scene const& get_scene() const;
private:
	sf::Vector2f velocity_;
	scene& scene_;
	std::size_t coins_;
};

#endif