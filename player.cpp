#include <SFML/Audio.hpp>
#include "scene.hpp"
#include "player.hpp"

player::player(object const& obj, scene& _scene):
	object(obj), scene_(_scene), coins_() {

}

void player::render(sf::RenderWindow &window) {
	sprite_.setPosition(rect_.left, rect_.top);

	window.draw(sprite_);
}

namespace {
	bool is_float_equal(float l, float r) {
		return std::abs(l - r) <= std::numeric_limits<float>::epsilon() * std::abs(l);
	}
}

void player::handle_event(sf::Event const& event) {
	const double speed = 4;
	switch (event.type) {
		case sf::Event::KeyPressed:
			switch (event.key.code) {
				case sf::Keyboard::W:
				case sf::Keyboard::Up:
				case sf::Keyboard::Space:
					if(is_float_equal(velocity_.y, 0)) {
						velocity_.y = -speed * 3;
					}
				break;

				case sf::Keyboard::D:
				case sf::Keyboard::Right:
					velocity_.x = speed;
				break;

				case sf::Keyboard::A:
				case sf::Keyboard::Left:
					velocity_.x = -speed;
				break;
                default:
                break;
			}
		break;
		case sf::Event::KeyReleased:
			switch (event.key.code) {
				case sf::Keyboard::W:
				case sf::Keyboard::Up:
					// velocity_.y = 0;
				break;

				case sf::Keyboard::D:
				case sf::Keyboard::Right:
					velocity_.x = 0;
				break;

				case sf::Keyboard::A:
				case sf::Keyboard::Left:
					velocity_.x = 0;
				break;
                default:
                break;				
			}
		break;
    default:
    break;
	};
}

void player::update(sf::Time const& delta) {
	static double const g = 1;
	velocity_.y += g;
	rect_.left += velocity_.x;// * delta.asMilliseconds();

	if(collision_info const info = scene_.check_collision(*this)) {
		if(info.obj->collision_type() == object::coin) {
			handle_coin(info);
		} else if(info.obj->collision_type() == object::solid) {
			rect_.left -= velocity_.x;
		}
	}

	rect_.top += velocity_.y;// * delta.asMilliseconds();
	if(collision_info const info = scene_.check_collision(*this)) {
		if(info.obj->collision_type() == object::coin) {
			handle_coin(info);
		} else if(info.obj->collision_type() == object::solid) {		
			rect_.top -= velocity_.y;

			if(info.place != collision_info::bottom) {
				velocity_.y = 0;
			} else {
				// velocity_.y += g;
			}
		}
	}
}

void player::handle_coin(collision_info const& info) {
	scene_.remove_object(info.obj);
	++coins_;

	static sf::Sound sound;
	static bool sound_loaded = false;
	if(!sound_loaded) {
		static sf::SoundBuffer buffer;
	    if (!buffer.loadFromFile("coin.ogg"))
	        throw std::runtime_error("No such file found coin.ogg");
	    sound.setBuffer(buffer);
	    sound_loaded = true;
	}
	sound.play();
}

std::size_t player::coins() const {
	return coins_;
}

scene const& player::get_scene() const {
	return scene_;
}

