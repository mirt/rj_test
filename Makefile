# SOURCES=main.cpp map.cpp object.cpp player.cpp scene.cpp hud.cpp tinyxml/tinyxml.cpp tinyxml/tinyxmlerror.cpp tinyxml/tinyxmlparser.cpp tinyxml/tinystr.cpp

LIBS=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
CXX := g++
OO=main.o map.o object.o player.o scene.o hud.o tinyxml/tinyxml.o game_rules.o tinyxml/tinyxmlerror.o tinyxml/tinyxmlparser.o tinyxml/tinystr.o

all: thegame

%.o: %.cpp
	$(CXX) -c $< -o $@

%.o: %.hpp
	$(CXX) -c $< -o $@

thegame: $(OO)
	@echo "** Building the game"
	$(CXX) -o thegame $(OO) $(LIBS)

clean:
	@echo "** Removing object files and executable..."
	rm -f thegame *.o