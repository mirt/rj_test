#include "scene.hpp"

scene::scene(map const& m, camera& cam):
	map_(m), camera_(cam) { 

	reload(map_);
}

struct not_player: public std::unary_function<object const&, bool>{
	bool operator()(object const& obj) const {
		return obj.name() != "player";
	}
};

namespace {
	template <class InputIterator, class OutputIterator, class Predicate>
	OutputIterator copy_if_(InputIterator first, InputIterator last,
	                       OutputIterator result, Predicate pred)
	{
	  return std::remove_copy_if(first,last,result,std::not1(pred));
	}
}

void scene::reload(map const& m) {
	player_.reset(new player(map_.get_player(), *this));
	camera_.set_target(player_);
	objects_.clear();
	copy_if_(map_.objects().begin(), map_.objects().end(), std::back_inserter(objects_), not_player());
}


void scene::render(sf::RenderWindow &window) {
	map_.render(window);
	// using boost::adaptors::transformed;
	for(objects_t::iterator it = objects_.begin(); it != objects_.end(); ++it) {
		it->render(window);
	}

	player_->render(window);
}

void scene::handle_event(sf::Event const& event) {
	player_->handle_event(event);
}

void scene::update(sf::Time const& delta) {
	player_->update(delta);
	camera_.update();
}

namespace { 
	collision_info check_collision(object const& obj_a, objects_t::iterator obj_b) {
		sf::Rect<int> const& A = obj_a.rect();
		sf::Rect<int> const& B = obj_b->rect();
		bool const result = A.intersects(B);	

		if(result) {
			if(B.contains(A.left, A.top) || B.contains(A.left + A.width, A.top) ||
			   B.contains(A.left, A.top + 1) || B.contains(A.left + A.width, A.top + 1)) {
				return collision_info::collision(collision_info::bottom, obj_b);
			} else {
				return collision_info::collision(collision_info::other, obj_b);
			}
		}
		
		return collision_info::no_collision();
	}
}

collision_info scene::check_collision(object const& obj) {
	for(objects_t::iterator it = objects_.begin(); it != objects_.end(); ++it) {
		if(collision_info const info = ::check_collision(obj, it))
			return info;
	}

	return collision_info::no_collision();
}

void scene::remove_object(objects_t::iterator it) {
	objects_.erase(it);
}

map const& scene::get_map() const {
	return map_;
}

player const& scene::get_player() const {
	assert(player_ && "player_ is empty");
	return *player_;
}

player& scene::get_player() {
	assert(player_ && "player_ is empty");
	return *player_;
}

camera const& scene::get_camera() const {
	return camera_;
}

collision_info::operator bool() const {
	return colided;
};

collision_info collision_info::collision(hit_place _place, objects_t::iterator _obj) {
	// assert(_place != none);
	return collision_info(true, _place, _obj);
}
collision_info collision_info::no_collision() {
	return collision_info(false);
}

collision_info::collision_info(bool _colided):
	colided(_colided), place(none) {
}

collision_info::collision_info(bool _colided, hit_place _place, objects_t::iterator _obj):
	colided(_colided), place(_place), obj(_obj) {
}

camera::camera(sf::Vector2i const& screen_size, sf::RenderWindow& window):
	view_(), screen_size_(screen_size), window_(window), target_(), pos_() {
    view_.reset(sf::FloatRect(0.0f, 0.0f, screen_size_.x, screen_size_.y));
    view_.setViewport(sf::FloatRect(0.0f, 0.0f, 2.0f, 2.0f));
}

void camera::update() {
	assert(target_ && "target_ is empty");
	if(target_->rect().left <  screen_size_.x / 4)
		pos_.x = screen_size_.x / 4;
	else if((target_->rect().left + screen_size_.x / 4) > target_->get_scene().get_map().width_px())
		pos_.x = target_->get_scene().get_map().width_px() - screen_size_.x / 4;
	else
		pos_.x = target_->rect().left;

	pos_.y = screen_size_.y / 2.5;
	view_.setCenter(x() + screen_size_.x / 4, y() + screen_size_.y / 4);
	window_.setView(view_);
}

void camera::set_target(player_ptr player) {
	target_ = player;
}

int camera::x() const {
	return pos_.x;
}

int camera::y() const {
	assert(target_ && "target_ is empty");
	return pos_.y;
}