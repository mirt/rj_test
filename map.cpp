#include <boost/lexical_cast.hpp>
#include "map.hpp"

map::map(std::string const& path) {
	load(path);
}

void map::load(std::string const& path) {
    TiXmlDocument file(path.c_str());

    if(!file.LoadFile()) {
        throw std::runtime_error((std::string("Failed to open file ") + path).c_str());
    }

    TiXmlElement const* const map_el = file.FirstChildElement("map");
    
    assert(map_el && "map_el is NULL");

    parse_map_properties(*map_el);
    load_tileset(*map_el);
    load_layers(*map_el);
    load_objects(*map_el);
}

void map::parse_map_properties(TiXmlElement const& map_el) {

    width_ = boost::lexical_cast<std::size_t>(map_el.Attribute("width"));
    height_ = boost::lexical_cast<std::size_t>(map_el.Attribute("height"));
    tile_width_ = boost::lexical_cast<std::size_t>(map_el.Attribute("tilewidth"));
    tile_height_ = boost::lexical_cast<std::size_t>(map_el.Attribute("tileheight"));
}

void map::load_tileset(TiXmlElement const& map_el) {
	TiXmlElement const* const tileset_el = map_el.FirstChildElement("tileset");
	assert(tileset_el && "tileset_el is NULL");
	TiXmlElement const* const image_el = tileset_el->FirstChildElement("image");
	assert(image_el && "image_el is NULL");

    first_tile_id_ = boost::lexical_cast<std::size_t>(tileset_el->Attribute("firstgid"));

	std::string const image_path = image_el->Attribute("source");

	sf::Image img;
    if(!img.loadFromFile(image_path)) {
    	throw std::runtime_error((std::string("Failed to load tileset ") + image_path).c_str());
    }

    img.createMaskFromColor(sf::Color(109, 159, 185));
    tileset_texture_.loadFromImage(img);
    tileset_texture_.setSmooth(false);
}

void map::load_layers(TiXmlElement const& map_el) {
	std::size_t const cols = tileset_texture_.getSize().x / tile_width_;
    std::size_t const rows = tileset_texture_.getSize().y / tile_height_;

    sub_rects_.reserve(cols * rows);

   for(std::size_t y = 0; y < rows; ++y) {
   		for(std::size_t x = 0; x < cols; ++x) {
	        sub_rects_.push_back(sf::Rect<int>(x * tile_width_, y * tile_height_, tile_width_, tile_height_));
    	}
    }
	
    for(TiXmlElement const* layer_el = map_el.FirstChildElement("layer"); layer_el; layer_el = layer_el->NextSiblingElement("layer")) {
    	layers_.push_back(layer(*layer_el, *this, sub_rects_));
    }

}

void map::load_objects(TiXmlElement const& map_el) {
	for(TiXmlElement const* obj_grp_el = map_el.FirstChildElement("objectgroup"); obj_grp_el; obj_grp_el = obj_grp_el->NextSiblingElement("objectgroup")) {
		for(TiXmlElement const* obj_el = obj_grp_el->FirstChildElement("object"); obj_el; obj_el = obj_el->NextSiblingElement("object")) {
			
			TiXmlElement const* const props_el = obj_el->FirstChildElement("properties");
			properties_t properties;
			if(props_el) {
                for(TiXmlElement const* prop_el = props_el->FirstChildElement("property"); prop_el; prop_el = prop_el->NextSiblingElement("property")) {
                    std::string const name = prop_el->Attribute("name");
                    std::string const value = prop_el->Attribute("value");
                    properties.insert(std::make_pair(name, value));
                }				
			}

			xml_object xml_obj(obj_el->Attribute("name") ? obj_el->Attribute("name"): "",
							   obj_el->Attribute("type") ? obj_el->Attribute("type"): "",
							   properties);

            float const x = boost::lexical_cast<float>(obj_el->Attribute("x"));
            float const y = boost::lexical_cast<float>(obj_el->Attribute("y"));

            sf::Sprite sprite;
            sprite.setTexture(tileset_texture_);
            sprite.setTextureRect(sf::Rect<int>(0,0,0,0));
            sprite.setPosition(x, y);

            std::size_t const width = obj_el->Attribute("width") ? boost::lexical_cast<std::size_t>(obj_el->Attribute("width")):
            					sub_rects_[boost::lexical_cast<int>(obj_el->Attribute("gid")) - first_tile_id_].width; 
            std::size_t const height = obj_el->Attribute("height") ? boost::lexical_cast<std::size_t>(obj_el->Attribute("height")):
            					sub_rects_[boost::lexical_cast<int>(obj_el->Attribute("gid")) - first_tile_id_].height;         
            if(obj_el->Attribute("width") == 0) {
                sprite.setTextureRect(sub_rects_[boost::lexical_cast<int>(obj_el->Attribute("gid")) - first_tile_id_]);
            }
            sf::Rect <int> rect(x, y, width, height);
			objects_.push_back(object(xml_obj, sprite, rect));
		}
	}
}

void map::render(sf::RenderWindow &window) const {
    for(std::vector<layer>::const_iterator layer = layers_.begin(); layer != layers_.end(); ++layer)
        for(std::vector<sf::Sprite>::const_iterator tile = layer->tiles.begin(); tile != layer->tiles.end(); ++tile)
            window.draw(*tile);
}

namespace {
    struct by_name {
        by_name(std::string const& _name):
            name(_name){
        }

        bool operator()(object const& obj) const {
            return obj.name() == name;
        }
        std::string name;
    };
}

// calls once at level start so it's ok
object map::get_player() const {
    objects_t::const_iterator const p = std::find_if(objects_.begin(), objects_.end(), by_name("player"));
    assert(p != objects_.end() && "No player object in map.");
    return *p;
}

map::objects_t const& map::objects() const {
    return objects_;
}

std::size_t map::width_px() const {
    return width_ * tile_width_;
}

std::size_t map::height_px() const {
    return height_ * tile_height_;
}

layer::layer(TiXmlElement const& layer_el, map const& m, std::vector<sf::Rect<int> > const& sub_rects):
	opacity(layer_el.Attribute("opacity") ? boost::lexical_cast<float>(layer_el.Attribute("opacity")) * 255: 255 ) {

	TiXmlElement const* const layer_data_el = layer_el.FirstChildElement("data");
	assert(layer_data_el && "layer_data_el is NULL");

	std::size_t x = 0;
	std::size_t y = 0;

	TiXmlElement const* tile_el = layer_data_el->FirstChildElement("tile");
	assert(tile_el && "tile_el is NULL");
    for(; tile_el; tile_el = tile_el->NextSiblingElement("tile")) {
    	tiles.push_back(load_sprite(*tile_el, m.tileset_texture_, sub_rects, m.first_tile_id_, x * m.tile_width_, y * m.tile_height_));

        x++;
        if (x >= m.width_) {
            x = 0;
            y++;
            if(y >= m.height_)
                y = 0;
        }    
    }

}

sf::Sprite layer::load_sprite(TiXmlElement const& tile_el, sf::Texture const&  tileset_texture, std::vector<sf::Rect<int> > const& sub_rects,
	std::size_t first_tile_id, std::size_t x, std::size_t y) {
    std::size_t const tile_gid = boost::lexical_cast<std::size_t>(tile_el.Attribute("gid"));
    std::size_t const sub_to_use = tile_gid - first_tile_id;

    sf::Sprite sprite;
    sprite.setTexture(tileset_texture);
    sprite.setTextureRect(sub_rects[sub_to_use]);
    sprite.setPosition(x, y);
    sprite.setColor(sf::Color(255, 255, 255, opacity));

	return sprite;
}

