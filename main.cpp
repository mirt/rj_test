#include <boost/exception/all.hpp>
#include <SFML/Graphics.hpp>
#include "scene.hpp"
#include "hud.hpp"
#include "game_rules.hpp"

void run() {
    map const m("level0.tmx");

    game_rules rules_;

    sf::Vector2i screen_size(800, 600);
    sf::VideoMode const video_mode(screen_size.x, screen_size.y, 32);
    sf::RenderWindow window(video_mode, "The Game");
    window.setFramerateLimit(60);

    camera camera_(screen_size, window);
    scene scene_(m, camera_);
    hud hud_(scene_, video_mode, rules_);

    sf::Clock clock;
    while (window.isOpen()) {

        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                break;
            
                case sf::Event::KeyPressed:
                    switch (event.key.code) {
                        case sf::Keyboard::Escape:
                            window.close();
                        break;
                        case sf::Keyboard::R:
                            scene_.reload(m);
                            hud_.reset();
                            rules_.reset();
                        break;
                        default:
                        break;
                    }
                break;
                default:
                break;
            }
            scene_.handle_event(event);
        }

        scene_.update(clock.restart());
        rules_.update(scene_);

        window.clear();
        scene_.render(window);
        hud_.render(window);
        window.display(); 
    }
}

int main(int, char*[])
{
    try {
        run();
    } catch(boost::exception const& e) {
        std::cerr << boost::diagnostic_information(e) << std::endl;
    } catch(std::exception const& e) {
        std::cerr << e.what() << std::endl;
    }
 
    return 0;
}