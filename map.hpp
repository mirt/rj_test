#ifndef MAP_HPP
#define MAP_HPP

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include "tinyxml/tinyxml.h"
#include "object.hpp"

class map;

struct layer {
	layer(TiXmlElement const& layer_el, map const& m, 
		std::vector<sf::Rect<int> > const& sub_rect);

    int opacity;
    std::vector<sf::Sprite> tiles;
private:
	sf::Sprite load_sprite(TiXmlElement const& tile_el, sf::Texture const&  tileset_texture, std::vector<sf::Rect<int> > const& sub_rects,
		std::size_t first_tile_id, std::size_t tile_width, std::size_t tile_height);
};

class map {
public:
	map(std::string const& path);
	void render(sf::RenderWindow &window) const;
    object get_player() const;

    typedef std::vector<object> objects_t;
    objects_t const& objects() const;
    
    std::size_t width_px() const;
    std::size_t height_px() const;
private:
    void load(std::string const& path);

    void parse_map_properties(TiXmlElement const& map_el);
    void load_tileset(TiXmlElement const& map_el);
    void load_layers(TiXmlElement const& map_el);
    void load_objects(TiXmlElement const& map_el);

    std::size_t width_, height_, tile_width_, tile_height_;
    std::size_t first_tile_id_;
    sf::Texture tileset_texture_;
    std::vector<layer> layers_;
    objects_t objects_;
    std::vector<sf::Rect<int> > sub_rects_;
    friend class layer;
};

#endif