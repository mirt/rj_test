#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <SFML/Graphics.hpp>

#include <string>
#include <vector>
#include <boost/unordered_map.hpp>

typedef boost::unordered_map<std::string, std::string> properties_t;

struct xml_object {
	xml_object(std::string name, std::string type, properties_t const& properties);
	std::string name, type;
	properties_t properties;
};

// class collisionable {
// public:
// };


class object {
public:
	enum collision_type_t {none, solid, coin, end_point};
	object(xml_object const& xml_object, sf::Sprite const& sprite, sf::Rect<int> const& rect);
	std::string name() const;
	sf::Rect<int> rect() const;
	collision_type_t collision_type() const;
	sf::Sprite sprite() const;

	void render(sf::RenderWindow &window);

protected:
	xml_object xml_object_;
	sf::Sprite sprite_;
	sf::Rect<int> rect_;
	collision_type_t collision_type_;
};

#endif