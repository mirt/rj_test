#ifndef GAME_RULES_
#define GAME_RULES_

class scene;

class game_rules {
public:
	enum game_state_t {playing, lost, won};

	game_rules();
	void update(scene& s);
	game_state_t game_state() const;
	void reset();
private:
	game_state_t state_;
	// scene& scene_;
};

#endif